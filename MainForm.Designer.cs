﻿namespace AzureSnapshots
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.comboResGroups = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.comboStorageAcc = new System.Windows.Forms.ComboBox();
            this.btnGetStorageAccounts = new System.Windows.Forms.Button();
            this.btnGetContainers = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.comboContainers = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.comboBlobs = new System.Windows.Forms.ComboBox();
            this.btnGetBlobs = new System.Windows.Forms.Button();
            this.btnSnapshot = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(91, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Resource groups:";
            // 
            // comboResGroups
            // 
            this.comboResGroups.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.comboResGroups.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboResGroups.FormattingEnabled = true;
            this.comboResGroups.Location = new System.Drawing.Point(116, 15);
            this.comboResGroups.Name = "comboResGroups";
            this.comboResGroups.Size = new System.Drawing.Size(327, 21);
            this.comboResGroups.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 74);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(94, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Storage accounts:";
            // 
            // comboStorageAcc
            // 
            this.comboStorageAcc.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.comboStorageAcc.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboStorageAcc.FormattingEnabled = true;
            this.comboStorageAcc.Location = new System.Drawing.Point(116, 71);
            this.comboStorageAcc.Name = "comboStorageAcc";
            this.comboStorageAcc.Size = new System.Drawing.Size(327, 21);
            this.comboStorageAcc.TabIndex = 4;
            // 
            // btnGetStorageAccounts
            // 
            this.btnGetStorageAccounts.Location = new System.Drawing.Point(116, 42);
            this.btnGetStorageAccounts.Name = "btnGetStorageAccounts";
            this.btnGetStorageAccounts.Size = new System.Drawing.Size(141, 23);
            this.btnGetStorageAccounts.TabIndex = 2;
            this.btnGetStorageAccounts.Text = "Get storage accounts";
            this.btnGetStorageAccounts.UseVisualStyleBackColor = true;
            this.btnGetStorageAccounts.Click += new System.EventHandler(this.btnGetStorageAccounts_Click);
            // 
            // btnGetContainers
            // 
            this.btnGetContainers.Location = new System.Drawing.Point(116, 99);
            this.btnGetContainers.Name = "btnGetContainers";
            this.btnGetContainers.Size = new System.Drawing.Size(141, 23);
            this.btnGetContainers.TabIndex = 5;
            this.btnGetContainers.Text = "Get containers";
            this.btnGetContainers.UseVisualStyleBackColor = true;
            this.btnGetContainers.Click += new System.EventHandler(this.btnGetContainers_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 133);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(62, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Cointainers:";
            // 
            // comboContainers
            // 
            this.comboContainers.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.comboContainers.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboContainers.FormattingEnabled = true;
            this.comboContainers.Location = new System.Drawing.Point(116, 130);
            this.comboContainers.Name = "comboContainers";
            this.comboContainers.Size = new System.Drawing.Size(327, 21);
            this.comboContainers.TabIndex = 7;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 189);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(36, 13);
            this.label4.TabIndex = 9;
            this.label4.Text = "Blobs:";
            // 
            // comboBlobs
            // 
            this.comboBlobs.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBlobs.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBlobs.FormattingEnabled = true;
            this.comboBlobs.Location = new System.Drawing.Point(116, 186);
            this.comboBlobs.Name = "comboBlobs";
            this.comboBlobs.Size = new System.Drawing.Size(327, 21);
            this.comboBlobs.TabIndex = 10;
            // 
            // btnGetBlobs
            // 
            this.btnGetBlobs.Location = new System.Drawing.Point(116, 157);
            this.btnGetBlobs.Name = "btnGetBlobs";
            this.btnGetBlobs.Size = new System.Drawing.Size(141, 23);
            this.btnGetBlobs.TabIndex = 8;
            this.btnGetBlobs.Text = "Get blobs";
            this.btnGetBlobs.UseVisualStyleBackColor = true;
            this.btnGetBlobs.Click += new System.EventHandler(this.btnGetBlobs_Click);
            // 
            // btnSnapshot
            // 
            this.btnSnapshot.Location = new System.Drawing.Point(116, 214);
            this.btnSnapshot.Name = "btnSnapshot";
            this.btnSnapshot.Size = new System.Drawing.Size(141, 23);
            this.btnSnapshot.TabIndex = 11;
            this.btnSnapshot.Text = "Create snapshot";
            this.btnSnapshot.UseVisualStyleBackColor = true;
            this.btnSnapshot.Click += new System.EventHandler(this.btnSnapshot_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(455, 251);
            this.Controls.Add(this.btnSnapshot);
            this.Controls.Add(this.btnGetBlobs);
            this.Controls.Add(this.comboBlobs);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.comboContainers);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.btnGetContainers);
            this.Controls.Add(this.btnGetStorageAccounts);
            this.Controls.Add(this.comboStorageAcc);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.comboResGroups);
            this.Controls.Add(this.label1);
            this.Name = "MainForm";
            this.Text = "Azure Snapshots";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox comboResGroups;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox comboStorageAcc;
        private System.Windows.Forms.Button btnGetStorageAccounts;
        private System.Windows.Forms.Button btnGetContainers;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox comboContainers;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox comboBlobs;
        private System.Windows.Forms.Button btnGetBlobs;
        private System.Windows.Forms.Button btnSnapshot;
    }
}

