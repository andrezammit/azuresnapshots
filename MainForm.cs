﻿using System;
using System.IO;
using System.Windows.Forms;
using System.Threading.Tasks;

using System.Collections.Generic;

using Microsoft.Azure.Management.Fluent;
using Microsoft.Azure.Management.Storage.Fluent;
using Microsoft.Azure.Management.ResourceManager.Fluent;
using Microsoft.Azure.Management.ResourceManager.Fluent.Core;

using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Auth;
using Microsoft.WindowsAzure.Storage.Blob;

namespace AzureSnapshots
{
    public partial class MainForm : Form
    {
        IAzure azure;

        List<IStorageAccount> cachedStorageAccounts = new List<IStorageAccount>();
        List<CloudBlobContainer> cachedContainers = new List<CloudBlobContainer>();

        public MainForm()
        {
            InitializeComponent();

            InitializeAzure();
            EnableControls();
        }

        private async void InitializeAzure()
        {
            await Task.Run((Action)LoginToAzure);
            GetResourceGroups();
        }

        private void LoginToAzure()
        {
            var credentials = SdkContext.
                AzureCredentialsFactory.
                FromFile("azure.json");

            azure = Microsoft.Azure.Management.Fluent.Azure
                .Configure()
                .Authenticate(credentials)
                .WithDefaultSubscription();
        }

        private async void GetResourceGroups()
        {
            var resourceGroups = await azure.ResourceGroups.ListAsync();
            FillCombo(comboResGroups, resourceGroups);
        }

        private async void GetStorageAccounts()
        {
            cachedStorageAccounts.Clear();

            var resourceGroup = (string) comboResGroups.SelectedItem;

            var storageAccounts = await azure.StorageAccounts.ListByResourceGroupAsync(resourceGroup);
            cachedStorageAccounts.AddRange(storageAccounts);

            FillCombo(comboStorageAcc, storageAccounts);
        }

        private CloudBlobClient GetBlobClient()
        {
            var index = comboStorageAcc.SelectedIndex;
            var storageAccount = cachedStorageAccounts[index];

            var accessKeys = storageAccount.GetKeys();

            var credentials = new StorageCredentials(storageAccount.Name,
                accessKeys[0].Value);

            var account = new CloudStorageAccount(credentials, true);
            var blobClient = account.CreateCloudBlobClient();

            return blobClient;
        }

        private async void GetContainers()
        {
            cachedContainers.Clear();

            var blobClient = GetBlobClient();

            var containers = await ListContainersAsync(blobClient);
            cachedContainers.AddRange(containers);

            comboContainers.Items.Clear();

            foreach (var container in containers)
            {
                comboContainers.Items.Add(container.Name);
            }

            if (comboContainers.Items.Count > 0)
            {
                comboContainers.SelectedIndex = 0;
            }

            EnableControls();
        }

        private async void CreateSnapshot()
        {
            var index = comboBlobs.SelectedIndex;
            var blobUri = (Uri) comboBlobs.SelectedItem;

            var blobClient = GetBlobClient();
           
            var blob = await blobClient.GetBlobReferenceFromServerAsync(blobUri);

            var blobName = Path.GetFileName(blobUri.AbsolutePath);
            var blockBlob = blob.Container.GetBlockBlobReference(blobName);

            var snapshotBlob = await blockBlob.CreateSnapshotAsync();

            MessageBox.Show("Created OS disk snapshot at " + snapshotBlob.SnapshotQualifiedUri, 
                "Create Snapshot", 
                MessageBoxButtons.OK, 
                MessageBoxIcon.Information);
        }

        private async void GetBlobs()
        {
            var index = comboContainers.SelectedIndex;
            var container = cachedContainers[index];
            
            var blobs = await ListBlobsAsync(container);

            comboBlobs.Items.Clear();

            foreach (var blob in blobs)
            {
                comboBlobs.Items.Add(blob.StorageUri.PrimaryUri);
            }

            if (comboBlobs.Items.Count > 0)
            {
                comboBlobs.SelectedIndex = 0;
            }

            EnableControls();
        }

        private async Task<List<CloudBlobContainer>> ListContainersAsync(CloudBlobClient blobClient)
        {
            BlobContinuationToken continuationToken = null;
            List<CloudBlobContainer> results = new List<CloudBlobContainer>();

            do
            {
                var response = await blobClient.ListContainersSegmentedAsync(continuationToken);
                continuationToken = response.ContinuationToken;

                results.AddRange(response.Results);
            }
            while (continuationToken != null);

            return results;
        }

        private async Task<List<IListBlobItem>> ListBlobsAsync(CloudBlobContainer container)
        {
            BlobContinuationToken continuationToken = null;
            List<IListBlobItem> results = new List<IListBlobItem>();

            do
            {
                var response = await container.ListBlobsSegmentedAsync(continuationToken);
                continuationToken = response.ContinuationToken;

                results.AddRange(response.Results);
            }
            while (continuationToken != null);

            return results;
        }

        private void FillCombo(ComboBox comboBox, IEnumerable<IHasName> contents)
        {
            comboBox.Items.Clear();

            foreach (var item in contents)
            {
                comboBox.Items.Add(item.Name);
            }

            if (comboBox.Items.Count > 0)
            {
                comboBox.SelectedIndex = 0;
            }

            EnableControls();
        }

        private void EnableControls()
        {
            comboResGroups.Enabled = comboResGroups.Items.Count != 0;
            comboStorageAcc.Enabled = comboStorageAcc.Items.Count != 0;
            comboContainers.Enabled = comboContainers.Items.Count != 0;
            comboBlobs.Enabled = comboBlobs.Items.Count != 0;

            btnGetStorageAccounts.Enabled = comboResGroups.Enabled;
            btnGetContainers.Enabled = comboStorageAcc.Enabled;
            btnGetBlobs.Enabled = comboContainers.Enabled;
            btnSnapshot.Enabled = comboBlobs.Items.Count != 0;
        }

        private void btnGetStorageAccounts_Click(object sender, EventArgs e)
        {
            GetStorageAccounts();
        }

        private void btnGetContainers_Click(object sender, EventArgs e)
        {
            GetContainers();
        }

        private void btnGetBlobs_Click(object sender, EventArgs e)
        {
            GetBlobs();
        }

        private void btnSnapshot_Click(object sender, EventArgs e)
        {
            CreateSnapshot();
        }
    }
}
